# vim: set ft=make :

.PHONY: appimage clean debug package release test

default:
	@echo "targets: appimage, clean, debug, package, release, test"

appimage:
	cmake -H. -Bbuilds/appimage -DCMAKE_INSTALL_PREFIX=/usr
	echo $(PATH)
	cd spar && cargo install --root ../builds/AppDir/usr --path .
	sed -i -e 's#/usr#././#g' builds/AppDir/usr/bin/spar
	cd builds/appimage && make -s install DESTDIR=../AppDir
	cd builds/appimage && make VERBOSE=1 appimage
	cd builds/appimage && md5sum *.AppImage > $$(ls *.AppImage).md5
	cd builds/appimage && sha256sum *.AppImage > $$(ls *.AppImage).sha256

clean:
	-rm -rf builds

debug:
	cmake -H. -Bbuilds/debug -DCMAKE_BUILD_TYPE=Debug
	cd builds/debug && make

package:
	git checkout-index --prefix=builds/source/ -a
	cmake -Hbuilds/source -Bbuilds/source
	cd builds/source && make package_source
	cd builds/source && md5sum *.bz2 > $$(ls *.bz2).md5
	cd builds/source && sha256sum *.bz2 > $$(ls *.bz2).sha256
	cd builds/source && md5sum *.xz > $$(ls *.xz).md5
	cd builds/source && sha256sum *.xz > $$(ls *.xz).sha256
	cd builds/source && md5sum *.zip > $$(ls *.zip).md5
	cd builds/source && sha256sum *.zip > $$(ls *.zip).sha256

release:
	cmake -H. -Bbuilds/release
	cd builds/release && make

test: debug
	cd builds/debug && make test
