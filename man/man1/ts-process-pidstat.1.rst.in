====================
 ts-process-pidstat
====================

----------------------
pidstat post-processor
----------------------

:Date: @MANDATE@
:Manual section: 1
:Manual group: Touchstone Tools @PROJECT_VERSION@ Documentation
:Version: Touchstone Tools @PROJECT_VERSION@

SYNOPSIS
========

**ts-process-pidstat** [option...]

DESCRIPTION
===========

**ts-process-pidstat** is a shell script massages **pidstat** human readable
output into a machine-readable CSV-like format.  This script will create a
*pidstat.csv* file in the same directory as the *pidstat.txt* file that was
created by **ts-sysstat**.

OPTIONS
=======

-i file  The *pidstat.txt* *file* created by **ts-sysstat**
-V, --version  Output version information, then exit.
--help  Show this help, then exit.

SEE ALSO
========

**ts-sysstat**\ (1), **pidstat**\ (1)
