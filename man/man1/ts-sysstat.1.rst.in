============
 ts-sysstat
============

-----------------------------------------------------
Stats collection management script and post-processor
-----------------------------------------------------

:Date: @MANDATE@
:Manual section: 1
:Manual group: Touchstone Tools @PROJECT_VERSION@ Documentation
:Version: Touchstone Tools @PROJECT_VERSION@

SYNOPSIS
========

**ts-sysstat** [option...] [command [args...]]

DESCRIPTION
===========

**ts-sysstat** is a wrapper script that starts and stops collecting data using
**sar**, **pidstat**, and **collectd** if they are available on the system and
in the user's path.  This script will save the output of these tools in a
specified directory.

There are two use cases:

1. Being capturing system statistics until explicitly stopped.
2. Capture system statistics for the duration of the given command.

When data collection is stopped, the script will also attempt to use **sadf**
with **sar** to generate human readable output files and SVG files.  The SVG
files are only generated if the host system's **sadf** supports it.

The data captured from **pidstat** (*pidstat.txt*) needs to be further massage
by **ts-process-pidstat** make it easier to be consumed by programs that can
handle CSV-like data.

See **ts-plot-pidstat** and **ts-plot-sar** for visualizing the collected data.

OPTIONS
=======

-i num  *num* seconds between samples, default 60
-o directory  Location to save data.
-s, --stop  Stop data collection processes.
-V, --version  Output version information, then exit.
--help  Show this help, then exit.

EXAMPLES
========

Explicitly starting and stopping and stopping data collection (note when
stopping data collection that the directory where data is being collected must
be specified)::

    ts-sysstat -o /tmp/data
    ts-sysstat -o /tmp/data -s

Collecting data for the duration of a given command.  This is an example of
collecting data for the duration of the 120 seconds of the sleep command::

    ts-sysstat -o /tmp/data sleep 120

Processing collected **pidstat** data requires a separate step since it can
done on any system at any time::

    ts-process-pidstat -i /tmp/data/pidstat.txt

SEE ALSO
========

**collectd**\ (1), **pidstat**\ (1), **sar**\ (1), **sadf**\ (1),
**ts-plot-pidstat**\ (1), **ts-plot-sar**\ (1), **ts-process-pidstat**\ (1)
